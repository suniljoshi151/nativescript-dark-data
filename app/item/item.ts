export class Item {
    id: number;
    name: string;
    role: string;
}

export class News {
    time: Date = new Date();
    uuid: string;
    headline: string;
    content: string;
    titleId: String;
    reaction: number;
    echo: number;
    summary: string;
    url: string;
    clusterUuid: number;
    related: null;
}

export class NewsCluster {
    id: number;
    uuid: string;
    security: string;
    date: Date = new Date();
    dailyChart: Chart;
    news: News[];
}

// here one thing you are sending date feild as a 'new Date()'
// it will be very good if you set that to current date
// because we can't set a string to a datatype variable
// i tried many times...
// ok, my fault, it was temp code O,kay  server will nprovide in the future
// thanks.... 
// so do you want to have a look at looks of applicatoin ui
// it is simple 
//ye splease , 
// download on my phone is too slow
// here i have 2 methods to show you screen let us check which will work well
// please wait for 1 min     ok
// on my mac I use Adnroid studio's s

export class Chart {
    constructor() {}
}

export class Relation {
    constructor() {}
}