"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var item_service_1 = require("./item.service");
var LoginComponent = (function () {
    function LoginComponent(routerExtensions, itemService) {
        this.routerExtensions = routerExtensions;
        this.itemService = itemService;
        this.user = "sunil";
        this.password = "password";
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.onTap = function () {
        console.log('i am tapped');
        this.itemService.loginUser(this.user, this.password);
        if (this.itemService.isLoggedIn) {
            this.routerExtensions.navigate(["/insights"], { clearHistory: true });
        }
        else {
            alert('please provide correct creds.');
        }
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        selector: "ns-login",
        moduleId: module.id,
        templateUrl: "./login.component.html",
        styleUrls: ['./login.component.css']
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, item_service_1.ItemService])
], LoginComponent);
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBQ2xELHNEQUErRDtBQUUvRCwrQ0FBNkM7QUFRN0MsSUFBYSxjQUFjO0lBSXZCLHdCQUFvQixnQkFBa0MsRUFBUyxXQUF3QjtRQUFuRSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQVMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFIdkYsU0FBSSxHQUFVLE9BQU8sQ0FBQztRQUN0QixhQUFRLEdBQVUsVUFBVSxDQUFDO0lBSTdCLENBQUM7SUFFRCxpQ0FBUSxHQUFSO0lBRUEsQ0FBQztJQUVELDhCQUFLLEdBQUw7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3JELEVBQUUsQ0FBQSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUMsV0FBVyxDQUFDLEVBQUUsRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUMxRSxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQztRQUMzQyxDQUFDO0lBQ0wsQ0FBQztJQUNMLHFCQUFDO0FBQUQsQ0FBQyxBQXJCRCxJQXFCQztBQXJCWSxjQUFjO0lBTjFCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsVUFBVTtRQUNwQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDbkIsV0FBVyxFQUFFLHdCQUF3QjtRQUNyQyxTQUFTLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQztLQUN2QyxDQUFDO3FDQUt3Qyx5QkFBZ0IsRUFBc0IsMEJBQVc7R0FKOUUsY0FBYyxDQXFCMUI7QUFyQlksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSAnbmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IEl0ZW0sIE5ld3NDbHVzdGVyfSBmcm9tIFwiLi9pdGVtXCI7XG5pbXBvcnQgeyBJdGVtU2VydmljZSB9IGZyb20gXCIuL2l0ZW0uc2VydmljZVwiO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJucy1sb2dpblwiLFxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gICAgdGVtcGxhdGVVcmw6IFwiLi9sb2dpbi5jb21wb25lbnQuaHRtbFwiLFxuICAgIHN0eWxlVXJsczogWycuL2xvZ2luLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBMb2dpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgdXNlcjpzdHJpbmcgPSBcInN1bmlsXCI7XG4gICAgcGFzc3dvcmQ6c3RyaW5nID0gXCJwYXNzd29yZFwiO1xuICAgIFxuICAgIGNvbnN0cnVjdG9yIChwdWJsaWMgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucywgcHVibGljIGl0ZW1TZXJ2aWNlOiBJdGVtU2VydmljZSkge1xuICAgICAgICBcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgXG4gICAgfVxuXG4gICAgb25UYXAoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdpIGFtIHRhcHBlZCcpO1xuICAgICAgICB0aGlzLml0ZW1TZXJ2aWNlLmxvZ2luVXNlcih0aGlzLnVzZXIsIHRoaXMucGFzc3dvcmQpO1xuICAgICAgICBpZih0aGlzLml0ZW1TZXJ2aWNlLmlzTG9nZ2VkSW4pIHtcbiAgICAgICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5uYXZpZ2F0ZShbXCIvaW5zaWdodHNcIl0sIHsgY2xlYXJIaXN0b3J5OiB0cnVlIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgYWxlcnQoJ3BsZWFzZSBwcm92aWRlIGNvcnJlY3QgY3JlZHMuJyk7XG4gICAgICAgIH1cbiAgICB9XG59Il19