import { Component } from "@angular/core";
import { RouterExtensions } from 'nativescript-angular/router';
import * as appSetting from 'application-settings';

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html",
})
export class AppComponent { 
    
    constructor(public routerExtensions: RouterExtensions) {
        console.log('this is constructor');
        console.log(appSetting.getBoolean('user'));
        if (appSetting.getBoolean('user')) {
            this.routerExtensions.navigate(["/insights"], { clearHistory: true })
        } else {
            this.routerExtensions.navigate(["/login"], { clearHistory: true});
        }
    }

 }
