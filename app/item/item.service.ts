import { Injectable } from "@angular/core";
import { Http, Headers, URLSearchParams, Response } from "@angular/http";
import { Item, NewsCluster } from "./item";
import * as appSetting from 'application-settings';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ItemService {
    clusterApiUrl: string = "http://demo0659361.mockable.io/athena/api/rest/v1/clusters/since/date/20170510094500";
    loginApiUrl: string = "https://demo0293055.mockable.io/athena/api/rest/v1/login/?";
    isLoggedIn = false;
    newsClusters: NewsCluster[] = [];
    user = {};
    constructor(public http: Http) { }

    fetchNewsClusters() {
        return this.http.get(this.clusterApiUrl)
        .map(data => data.json().clusters as NewsCluster[]);
    }

    getNewsCluster(): NewsCluster[] {
        return this.newsClusters;
    }

    loginUser(username: string, password: string) {
        this.loginApiUrl += "user="+username+"&pwd="+password;
        let body = new URLSearchParams();
        body.set('user', username);
        body.set('pass', password);
        let header = new Headers();
        header.append('Content-Type', 'application/x-www-form-urlencoded');
        this.http.post(this.loginApiUrl, body).map((response:Response) => {
            return response.json();
        }).subscribe( (response: Response) => {
            this.user = JSON.parse(JSON.stringify(response)).data;
            console.dir(this.user);
            appSetting.setBoolean('user', true);
            this.isLoggedIn = true;
        })
    }
    // here is our service that make calls to server
    // we provide our endpoints here and make a call to serve
    // with fetchNewsClusters method.... now comes our skeleton class body 
    // i will show you once you confirm this....
    // okay

    private items = new Array<Item>(
        { id: 1, name: "Ter Stegen", role: "Goalkeeper" },
        { id: 3, name: "Piqué", role: "Defender" },
        { id: 4, name: "I. Rakitic", role: "Midfielder" },
        { id: 5, name: "Sergio", role: "Midfielder" },
        { id: 6, name: "Denis Suárez", role: "Midfielder" },
        { id: 7, name: "Arda", role: "Midfielder" },
        { id: 8, name: "A. Iniesta", role: "Midfielder" },
        { id: 9, name: "Suárez", role: "Forward" },
        { id: 10, name: "Messi", role: "Forward" },
        { id: 11, name: "Neymar", role: "Forward" },
        { id: 12, name: "Rafinha", role: "Midfielder" },
        { id: 13, name: "Cillessen", role: "Goalkeeper" },
        { id: 14, name: "Mascherano", role: "Defender" },
        { id: 17, name: "Paco Alcácer", role: "Forward" },
        { id: 18, name: "Jordi Alba", role: "Defender" },
        { id: 19, name: "Digne", role: "Defender" },
        { id: 20, name: "Sergi Roberto", role: "Midfielder" },
        { id: 21, name: "André Gomes", role: "Midfielder" },
        { id: 22, name: "Aleix Vidal", role: "Midfielder" },
        { id: 23, name: "Umtiti", role: "Defender" },
        { id: 24, name: "Mathieu", role: "Defender" },
        { id: 25, name: "Masip", role: "Goalkeeper" },
    );

    getItems(): Item[] {
        return this.items;
    }

    getItem(id: number): Item {
        return this.items.filter(item => item.id === id)[0];
    }
}
