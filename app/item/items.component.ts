import { Component, OnInit } from "@angular/core";
import * as appSetting from 'application-settings';
import { RouterExtensions } from 'nativescript-angular/router';
import { Item, NewsCluster} from "./item";
import { ItemService } from "./item.service";

@Component({
    selector: "ns-items",
    moduleId: module.id,
    templateUrl: "./items.component.html"
})
export class ItemsComponent implements OnInit {
    items: Item[];
    newsClusters: NewsCluster[];
    constructor(private itemService: ItemService, public routerExtensions: RouterExtensions) { 
    	this.itemService.fetchNewsClusters().subscribe(data => {
    		this.newsClusters = data;
    		console.dir(data);
    		console.log('date experiment ss', this.newsClusters)
    	});
    }

    ngOnInit(): void {
        this.items = this.itemService.getItems();
    }

    logout() {
        console.log('I am getting tapped...');
        appSetting.remove('user');
        this.routerExtensions.navigate(["/login"], { clearHistory: true});
    }
}
