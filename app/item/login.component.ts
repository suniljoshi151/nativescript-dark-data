import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from 'nativescript-angular/router';
import { Item, NewsCluster} from "./item";
import { ItemService } from "./item.service";

@Component({
    selector: "ns-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    user:string = "sunil";
    password:string = "password";
    
    constructor (public routerExtensions: RouterExtensions, public itemService: ItemService) {
        
    }

    ngOnInit() {
        
    }

    onTap() {
        console.log('i am tapped');
        this.itemService.loginUser(this.user, this.password);
        if(this.itemService.isLoggedIn) {
            this.routerExtensions.navigate(["/insights"], { clearHistory: true });
        } else {
            alert('please provide correct creds.');
        }
    }
}